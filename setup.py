from setuptools import setup

setup(
    name='postgres_slave_http_monitor',
    version='0.2',
    description='Simple to expose using http the current status of a postgres slave replication',
    # url='http://github.com/storborg/funniest',
    # author='Flying Circus',
    # author_email='flyingcircus@example.com',
    license='MIT',
    packages=['postgres_slave_http_monitor'],
    scripts=['start_postgres_slave_http_monitor.py'],
    zip_safe=False,
    install_requires=['flask>=0.12']
)
