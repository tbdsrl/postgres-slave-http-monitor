import logging
import subprocess
from flask import Flask
app = Flask(__name__)


LAG_QUERY = "SELECT CASE WHEN pg_last_xlog_receive_location() = pg_last_xlog_replay_location() THEN 0 ELSE EXTRACT (EPOCH FROM now() - pg_last_xact_replay_timestamp())::INTEGER END AS replication_lag;"


class UnknowStateException(Exception):
    pass


def _is_the_node_a_slave():
    p = subprocess.Popen(['psql', 'template1', '-U', 'replication', '-t', '-P', 'format=unaligned', '-c', 'select pg_is_in_recovery()'],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    if out.strip() == 't':
        return True
    elif out.strip() == 'f':
        return False
    else:
        logging.error('_is_the_node_a_slave out, %s', out)
        logging.error('_is_the_node_a_slave err, %s', err)
        raise UnknowStateException('Could not determine the current status of postgres out: {} err: {}'.format(out, err))


@app.route("/check_slave_health")
def check_health():
    try:
        is_slave = _is_the_node_a_slave()
        if is_slave:
            p = subprocess.Popen(['psql', 'template1', '-U', 'replication', '-t', '-P', 'format=unaligned', '-c', LAG_QUERY],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            if int(out) > 60:
                return 'Postgres is out of sync by {}'.format(out), 503

            return 'Healty', 200
        else:
            return 'Postgres is not in recovery', 503
    except UnknowStateException, exc:
        return 'Could not determine the current status of postgres {}'.format(exc), 503



@app.route("/check_master_health")
def check_master_health():
    try:
        is_slave = _is_the_node_a_slave()
        if not is_slave:
            return 'Healty', 200
        else:
            return 'Postgres is in recovery', 503
    except UnknowStateException, exc:
        return 'Could not determine the current status of postgres {}'.format(exc), 503

def main():
    app.run(host='0.0.0.0', port=5433)


if __name__ == "__main__":
    main()
